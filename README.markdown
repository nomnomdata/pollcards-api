To work with this code locally:

1. Download the following package dependencies locally (to deploy to app engine, the folder structure must be under the application's root and must match the package name):
	code.google.com/p/freetype-go/freetype
	github.com/garyburd/go-oauth/oauth
	github.com/gorilla/mux
	github.com/nu7hatch/gouuid

2. Create a client application with Twitter:
	a. Set the application's Access Level to Read/Write (to allow the app to tweet on the user's behalf)
	b. Set the application's Sign in with Twitter setting to Yes
	c. Edit the local version of config.json and set the values of Token and Secret to Consumer Key (API Key) and Consumer Secret (API Secret) respectively.

To run the application locally:

0. Set the value of FrontEndUrl in config.json to the URL of the local instance of the web application that will be consuming the API.
1. goapp serve


User Authentication Flow:
1. User clicks a "Login with Twitter" button in the web application.
2. Web application makes a request to the /user/login/twitter API method exposed by the API server, providing a callback URL, e.g. /user/login/twitter?redirect=http://blah.blah.com/MY_LOGGED_IN_URL
3. API server redirects the user to Twitter, where the user 
	a. Enters their login credentials if they are not currently logged into twitter
	b. Grants the application permissions to read and post on their behalf using the Twitter API
4. After completing the login flow at Twitter, Twitter redirects the user back to the API server, which either creates a new user entity and stores it to the datastore if one doesn't already exist for the current user, or else
retrieves the user entity from the datastore based on the user's unique Twitter id value.
5. The API server redirects the user back to the callback specified by the web application, providing the unique UUID associated with the user entity (the pollcards user id) on the querystring.
6. The web application will need to retain this UUID value, as it must be passed on each web API request call. 

Web API Authentication/Authorization Flow:
1. For secure API methods (ones that require a user context), the web application creates a request, setting the Authorization header to the current user's UUID value (the one passed back from the User Authentication flow above).
2. Web application sends request to API server.
3. For secure API methods, the API server gets user's UUID value from the authorization header and retrieves the corresponding user entity from the datastore.
4. Web API server performs the request, returning a JSON response back to the web application as appropriate.

