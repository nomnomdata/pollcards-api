package app

import (
	"encoding/json"
)

func getAppConfigHandler(c *context) error {
	jsonStr, err := json.Marshal(appConfig)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}
