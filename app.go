package app

import (
	"github.com/gorilla/mux"
	"net/http"
)

//TODO: Use https for secure routes
func init() {
	appConfig.LoadFromFile()
	//create the gorilla router
	r := mux.NewRouter()
	apiUri := appConfig.ApiUri()
	//public api routes
	r.Handle(apiUri+"/config", handler(configGetHandler)).Methods("GET", "OPTIONS")
	r.Handle(apiUri+"/poll/search", handler(pollSearchHandler)).Methods("GET", "OPTIONS")
	//r.Handle(apiUri+"/user/search", handler(userSearchHandler)).Methods("GET")
	r.Handle(apiUri+"/poll/{id}", handler(pollGetHandler)).Methods("GET", "OPTIONS")
	r.Handle(apiUri+"/poll/image/{id}", handler(pollGetImageHandler)).Methods("GET", "OPTIONS")
	r.Handle(apiUri+"/poll/{id}/{answerIdx}", handler(pollGetAnswerHandler)).Methods("GET", "OPTIONS")
	//secure api routes (user context required)
	r.Handle(apiUri+"/user", &authHandler{handler: userGetHandler}).Methods("GET", "OPTIONS")
	r.Handle(apiUri+"/user/login/twitter", handler(serveTwitterLogin)).Methods("GET", "OPTIONS")
	r.Handle("/user/login/twitter/callback", handler(serveTwitterCallback))
	r.Handle(apiUri+"/poll", &authHandler{handler: pollCreateHandler}).Methods("POST", "OPTIONS")
	r.Handle(apiUri+"/poll", &authHandler{handler: pollGetAllHandler}).Methods("GET", "OPTIONS")
	r.Handle(apiUri+"/poll/tweet/{id}", &authHandler{handler: pollTweetHandler}).Methods("POST", "OPTIONS")
	r.Handle(apiUri+"/vote/{pollId}/{answerIdx}", &authHandler{handler: voteCreateHandler}).Methods("POST", "OPTIONS")
	r.Handle(apiUri+"/vote/tweet/{pollId}/{answerIdx}", &authHandler{handler: voteTweetHandler}).Methods("POST", "OPTIONS")
	//admin api routes (admin user context required)
	r.Handle(apiUri+"/admin/app/config", &authHandlerAdmin{handler: getAppConfigHandler}).Methods("GET", "OPTIONS")
	//this has to be the last route, otherwise it will take precedence over all of the other routes
	r.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	//assign the gorilla router as the default
	http.Handle("/", r)
}
