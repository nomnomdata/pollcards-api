package app

import (
	"appengine"
	"encoding/json"
	"net/http"
)

//stores context associated with an HTTP request.
type context struct {
	c appengine.Context
	r *http.Request
	w http.ResponseWriter
	u UserProfileMeta //user info for quick access
}

func (c context) isUserAuthenticated() bool {
	return len(c.u.KeyStr) > 0
}

type AppError struct {
	Message string
	Code    int
}

func ErrorResponse(w http.ResponseWriter, err error, code int) {
	appError := AppError{
		Message: err.Error(),
		Code:    code,
	}
	jsonStr, _ := json.Marshal(appError)
	w.Header().Set("Content-type", "application/json")
	w.WriteHeader(code)
	w.Write(jsonStr)
}

type JsonResponse map[string]interface{}

func (r JsonResponse) Bytes() []byte {
	b, err := json.Marshal(r)
	if err != nil {
		return nil
	}
	return b
}

func (r JsonResponse) String() (s string) {
	b, err := json.Marshal(r)
	if err != nil {
		s = "Bad Json Object"
		return
	}
	s = string(b)
	return
}

//handler used to serve public routes (does not require authenticated users)
type handler func(c *context) error

func (h handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	c := context{
		c: ctx,
		r: r,
		w: w,
	}
	if c.r.Method == "OPTIONS" {
		getOptionsHandler(&c)
		return
	}
	c.w.Header().Add("Access-Control-Allow-Origin", appConfig.FrontEndUrl)
	err := h(&c)
	if err != nil {
		ErrorResponse(w, err, 500)
	}
}

//handler used to serve secure api routes (requires authenticated users; unauthenticated users will receive an error in the response)
type authHandler struct {
	handler handler
}

func (h *authHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	c := context{
		c: ctx,
		r: r,
		w: w,
	}
	if c.r.Method == "OPTIONS" {
		getOptionsHandler(&c)
		return
	}
	var ur UserRepository
	err := ur.getUserFromAuthHeader(&c)
	if err != nil {
		ErrorResponse(w, err, 401)
		return
	}
	c.w.Header().Add("Access-Control-Allow-Origin", appConfig.FrontEndUrl)
	err = h.handler(&c)
	if err != nil {
		ErrorResponse(w, err, 500)
	}
}

//handler used to serve secure admin routes (requires authenticated admin users; unauthenticated users and non-admin users will receive an error in the response)
type authHandlerAdmin struct {
	handler handler
}

func (h *authHandlerAdmin) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ctx := appengine.NewContext(r)
	c := context{
		c: ctx,
		r: r,
		w: w,
	}
	if c.r.Method == "OPTIONS" {
		getOptionsHandler(&c)
		return
	}
	var ur UserRepository
	err := ur.getUserFromAuthHeader(&c)
	if err != nil || !c.u.IsAdmin {
		ErrorResponse(w, err, 401)
		return
	}
	c.w.Header().Add("Access-Control-Allow-Origin", appConfig.FrontEndUrl)
	err = h.handler(&c)
	if err != nil {
		ErrorResponse(w, err, 500)
	}
}

func getOptionsHandler(c *context) error {
	c.w.Header().Set("Access-Control-Allow-Origin", appConfig.FrontEndUrl)
	c.w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	c.w.Header().Set("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, Authorization")
	c.w.WriteHeader(200)
	return nil
}
