package app

import (
	"encoding/json"
	"github.com/garyburd/go-oauth/oauth"
	"io/ioutil"
)

type ApiConfig struct {
	MaxTweetLength int
	CreditConfig   CreditConfig
}

type AppConfig struct {
	ApiConfig
	FrontEndUrl string
	ApiVersion  string
	Credentials oauth.Credentials
}

func (a AppConfig) ApiUri() string {
	return "/api/" + a.ApiVersion
}

func (a AppConfig) AppUrl(c *context) string {
	scheme := "http"
	if len(c.r.URL.Scheme) > 0 {
		scheme = c.r.URL.Scheme
	}
	return scheme + "://" + c.r.Host
}

var appConfig = AppConfig{}

func (a AppConfig) LoadFromFile() {
	//read config file with app-specific oauth credentials
	b, err := ioutil.ReadFile("config.json")
	if err != nil {
		panic(err)
	}
	//	if err := json.Unmarshal(b, &oauthClient.Credentials); err != nil {
	if err := json.Unmarshal(b, &appConfig); err != nil {
		panic(err)
	}
	oauthClient.Credentials = appConfig.Credentials
}
