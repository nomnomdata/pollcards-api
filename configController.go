package app

import (
	"encoding/json"
)

func configGetHandler(c *context) error {
	jsonStr, err := json.Marshal(appConfig.ApiConfig)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}
