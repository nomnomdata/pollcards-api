package app

import (
	"appengine/datastore"
	"time"
)

type CreditConfig struct {
	NewUser     int
	CreatePoll  int
	RetweetPoll int
	Vote        int
}

type CreditTransaction struct {
	UserKey    *datastore.Key `json:"-"`
	CreatedOn  time.Time
	NumCredits int
}

func adjustCredits(c *context, credits int) (int, error) {
	var u UserInfo
	err := datastore.Get(c.c, c.u.Key, &u)
	if err != nil {
		return 0, err
	}
	u.Credits = u.Credits + credits
	_, err = datastore.Put(c.c, c.u.Key, &u)
	if err != nil {
		return 0, err
	}
	return u.Credits, nil
}
