package app

import (
	"bytes"
	"code.google.com/p/freetype-go/freetype"
	"flag"
	"image"
	"image/draw"
	"image/jpeg"
	_ "image/png"
	"io/ioutil"
	"os"
	"strings"
)

var (
	dpi             = flag.Float64("dpi", 72, "screen resolution in Dots Per Inch")
	fontfile        = flag.String("fontfile", "helvetica-neue-light-italic.ttf", "filename of the ttf font")
	fontsize        = flag.Float64("fontsize", 26, "font size in points")
	linespacing     = flag.Float64("linespacing", 1.5, "line spacing (e.g. 2 means double spaced)")
	imagefile       = flag.String("imagefile", "poll_image.png", "filename of the background image")
	imageheight     = flag.Int("imageheight", 275, "height of the background image in pixels")
	imagewidth      = flag.Int("imagewidth", 500, "width of the background image in pixels")
	imageleftmargin = flag.Int("imageleftmargin", 40, "left margin of the image text over the background image")
	imagetopmargin  = flag.Int("imagetopmargin", 40, "top margin of the image text over the background image")
	maxlinelength   = flag.Int("maxlinelength", 35, "maximum line length in characters")
)

func getImageBytes(c *context, imageText string) ([]byte, error) {
	imageRGBA, err := getImage(c, imageText)
	if err != nil {
		return nil, err
	}
	//convert RGBA to byte array
	imgBuf := new(bytes.Buffer)
	err = jpeg.Encode(imgBuf, imageRGBA, nil)
	if err != nil {
		return nil, err
	}
	return imgBuf.Bytes(), nil
}

func getImage(c *context, imageText string) (*image.RGBA, error) {
	flag.Parse()
	//read the image file
	reader, err := os.Open(*imagefile)
	if err != nil {
		return nil, err
	}
	src, _, err := image.Decode(reader)
	if err != nil {
		return nil, err
	}
	b := src.Bounds()
	rgba := image.NewRGBA(image.Rect(0, 0, *imagewidth, *imageheight))
	draw.Draw(rgba, rgba.Bounds(), src, b.Min, draw.Src)
	// Read the font data.
	fontBytes, err := ioutil.ReadFile(*fontfile)
	if err != nil {
		return nil, err
	}
	font, err := freetype.ParseFont(fontBytes)
	if err != nil {
		return nil, err
	}
	// Initialize the context.
	fg := image.Black
	ftc := freetype.NewContext()
	ftc.SetHinting(freetype.NoHinting)
	ftc.SetDPI(*dpi)
	ftc.SetFont(font)
	ftc.SetClip(rgba.Bounds())
	ftc.SetDst(rgba)
	ftc.SetSrc(fg)

	// Draw the text.
	sz := *fontsize
	pt := freetype.Pt(*imageleftmargin, *imagetopmargin+int(ftc.PointToFix32(sz)>>8))
	ftc.SetFontSize(sz)
	lines := getImageTextLines(imageText)
	for _, line := range lines {
		_, err = ftc.DrawString(line, pt)
		if err != nil {
			return nil, err
		}
		pt.Y += ftc.PointToFix32(sz * *linespacing)
	}
	return rgba, nil
}

func getImageTextLines(imageText string) []string {
	//split text into individual words
	words := strings.Split(imageText, " ")
	lines := make([]string, 0, len(words))
	line := ""
	//for each word
	for _, word := range words {
		//append next word to line
		appendToLine := word
		//if we're not at the beginning of the line, add a space character before the word
		if len(line) > 0 {
			appendToLine = " " + appendToLine
		}
		//if the length of the current line plus the next word to append is less than the maximum line length, add it on; otherwise, create a new line
		if len(line+appendToLine) <= *maxlinelength {
			line += appendToLine
		} else {
			//append this line to the array of lines
			lines = append(lines, line)
			//start a new line with the current word
			line = word
		}
	}
	//append the last word if necessary
	if len(line) > 0 {
		lines = append(lines, line)
	}
	return lines
}
