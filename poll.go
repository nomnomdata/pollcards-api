package app

//TODO: Add caching

import (
	"appengine/datastore"
	"encoding/json"
	"github.com/nu7hatch/gouuid"
	"time"
)

type Poll struct {
	Question    string
	Answers     []string
	Description string
	UserKey     *datastore.Key `json:"-"`
	CreatedOn   time.Time
	ExpiresOn   time.Time
	IsPrivate   bool
	TwitterId   string
}

type AnswerDetail struct {
	AnswerId  int
	Answer    string
	VoteCount int
}

type PollMeta struct {
	Key         *datastore.Key `json:"-"`
	KeyStr      string
	User        UserMeta
	Question    string
	Answers     []AnswerDetail
	Description string
	CreatedOn   time.Time
	ExpiresOn   time.Time
	IsPrivate   bool
	TwitterId   string
}

type AnswerMeta struct {
	PollKey    *datastore.Key `json:"-"`
	PollKeyStr string
	Index      int
	Users      []UserMeta
}

func (p PollMeta) CreatedOnFmt() string {
	return p.CreatedOn.Format("01/02/2006 15:04")
}

func (p PollMeta) Url(c *context) string {
	return appConfig.AppUrl(c) + appConfig.ApiUri() + "/poll/" + p.KeyStr
}

func (p PollMeta) ImageUrl(c *context) string {
	return appConfig.AppUrl(c) + appConfig.ApiUri() + "/poll/image/" + p.KeyStr
}

func (p PollMeta) JsonStr() (s string) {
	b, err := json.Marshal(p)
	if err != nil {
		s = "Bad Json Object"
		return
	}
	s = string(b)
	return
}

type PollRepository struct {
}

func (pr PollRepository) create(c *context, p Poll) (PollMeta, error) {
	u4, err := uuid.NewV4()
	if err != nil {
		return PollMeta{}, err
	}
	u4Str := u4.String()
	key := datastore.NewKey(c.c, "poll", u4Str, 0, nil)
	p.UserKey = c.u.Key
	p.CreatedOn = time.Now()
	p.ExpiresOn = p.CreatedOn.AddDate(0, 0, 1)
	p.IsPrivate = false
	_, err = datastore.Put(c.c, key, &p)
	if err != nil {
		return PollMeta{}, err
	}
	adjustCredits(c, appConfig.CreditConfig.CreatePoll)
	pollMeta, err := createPollMeta(c, key, p)
	if err != nil {
		return PollMeta{}, err
	}
	return pollMeta, nil
}

func (pr PollRepository) update(c *context, pm PollMeta) (PollMeta, error) {
	p, err := createPoll(c, pm)
	if err != nil {
		return PollMeta{}, err
	}
	_, err = datastore.Put(c.c, pm.Key, &p)
	if err != nil {
		return PollMeta{}, err
	}
	return pm, nil
}

func (pr PollRepository) getAll(c *context) ([]PollMeta, error) {
	var polls []Poll
	q := datastore.NewQuery("poll").Filter("UserKey =", c.u.Key)
	keys, err := q.GetAll(c.c, &polls)
	if err != nil {
		return []PollMeta{}, err
	}
	pollsMeta := make([]PollMeta, len(keys))
	for i, p := range polls {
		pollMeta, err := createPollMeta(c, keys[i], p)
		if err != nil {
			return []PollMeta{}, err
		}
		pollsMeta[i] = pollMeta
	}
	return pollsMeta, nil
}

func (pr PollRepository) get(c *context, id string) (PollMeta, error) {
	var p Poll
	key := datastore.NewKey(c.c, "poll", id, 0, nil)
	err := datastore.Get(c.c, key, &p)
	if err != nil {
		return PollMeta{}, err
	}
	meta, err := createPollMeta(c, key, p)
	if err != nil {
		return PollMeta{}, err
	}
	return meta, nil
}

func (pr PollRepository) getAnswer(c *context, id string, answerIdx int) (AnswerMeta, error) {
	var answerMeta AnswerMeta
	answerMeta.PollKeyStr = id
	answerMeta.Index = answerIdx
	pollMeta, err := pr.get(c, id)
	if err != nil {
		return AnswerMeta{}, err
	}
	var vr VoteRepository
	votesMeta, err := vr.getAllByAnswer(c, pollMeta.Key, answerIdx)
	var ur UserRepository
	answerMeta.Users = make([]UserMeta, len(votesMeta))
	for i, v := range votesMeta {
		userMeta, err := ur.getUser(c, v.UserKey)
		if err != nil {
			return AnswerMeta{}, nil
		}
		answerMeta.Users[i] = userMeta
	}
	return answerMeta, nil
}

func (pr PollRepository) search(c *context) ([]PollMeta, error) {
	var polls []Poll
	q := datastore.NewQuery("poll")
	keys, err := q.GetAll(c.c, &polls)
	if err != nil {
		return nil, err
	}
	pollsMeta := make([]PollMeta, len(keys))
	for i, p := range polls {
		pollMeta, err := createPollMeta(c, keys[i], p)
		if err != nil {
			return nil, err
		}
		pollsMeta[i] = pollMeta
	}
	return pollsMeta, nil
}

func createPollMeta(c *context, key *datastore.Key, p Poll) (PollMeta, error) {
	var ur UserRepository
	userMeta, err := ur.getUser(c, p.UserKey)
	if err != nil {
		return PollMeta{}, err
	}
	var voteRepos VoteRepository
	votesMeta, err := voteRepos.getAll(c, key)
	if err != nil {
		return PollMeta{}, err
	}
	var answerDetail = make([]AnswerDetail, len(p.Answers))
	for i, a := range p.Answers {
		answerDetail[i].AnswerId = i
		answerDetail[i].Answer = a
	}
	for _, v := range votesMeta {
		answerDetail[v.AnswerIdx].VoteCount++
	}
	pollMeta := PollMeta{
		Key:         key,
		KeyStr:      key.StringID(),
		User:        userMeta,
		Question:    p.Question,
		Answers:     answerDetail,
		Description: p.Description,
		CreatedOn:   p.CreatedOn,
		ExpiresOn:   p.ExpiresOn,
		IsPrivate:   p.IsPrivate,
		TwitterId:   p.TwitterId,
	}
	return pollMeta, nil
}

func createPoll(c *context, p PollMeta) (Poll, error) {
	answers := make([]string, len(p.Answers))
	for i, a := range p.Answers {
		answers[i] = a.Answer
	}
	poll := Poll{
		Question:    p.Question,
		Answers:     answers,
		Description: p.Description,
		UserKey:     p.User.Key,
		CreatedOn:   p.CreatedOn,
		ExpiresOn:   p.ExpiresOn,
		IsPrivate:   p.IsPrivate,
		TwitterId:   p.TwitterId,
	}
	return poll, nil
}
