package app

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"image/jpeg"
	"io/ioutil"
	"strconv"
)

func pollCreateHandler(c *context) error {
	var pr PollRepository
	var p Poll
	body, _ := ioutil.ReadAll(c.r.Body)
	json.Unmarshal(body, &p)
	pkv, err := pr.create(c, p)
	if err != nil {
		return err
	}
	jsonStr, err := json.Marshal(pkv)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}

func pollGetAllHandler(c *context) error {
	var pr PollRepository
	pollsMeta, err := pr.getAll(c)
	if err != nil {
		return err
	}
	jsonStr, err := json.Marshal(pollsMeta)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}

func pollGetHandler(c *context) error {
	var pr PollRepository
	vars := mux.Vars(c.r)
	id := vars["id"]
	pollMeta, err := pr.get(c, id)
	if err != nil {
		return err
	}
	jsonStr, err := json.Marshal(pollMeta)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}

func pollGetAnswerHandler(c *context) error {
	var pr PollRepository
	vars := mux.Vars(c.r)
	id := vars["id"]
	answerIdx, err := strconv.Atoi(vars["answerIdx"])
	if err != nil {
		return err
	}
	answerMeta, err := pr.getAnswer(c, id, answerIdx)
	if err != nil {
		return err
	}
	jsonStr, err := json.Marshal(answerMeta)
	if err != nil {
		return err
	}
	c.c.Infof("jsonStr: %v", jsonStr)
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}

func pollGetImageHandler(c *context) error {
	c.c.Infof("Entering Image Handler")
	var pr PollRepository
	vars := mux.Vars(c.r)
	id := vars["id"]
	c.c.Infof("Getting PollMeta")
	pollMeta, err := pr.get(c, id)
	if err != nil {
		return err
	}
	c.c.Infof("Getting Image")
	img, err := getImage(c, pollMeta.Question)
	if err != nil {
		return err
	}
	c.c.Infof("Got Image")
	// Encode JPEG image and write it as the response.
	c.w.Header().Set("Content-type", "image/png")
	c.w.Header().Set("cache-control", "public, max-age=259200")
	jpeg.Encode(c.w, img, nil)
	return nil
}

func pollTweetHandler(c *context) error {
	//get poll from datastore by id
	var pr PollRepository
	vars := mux.Vars(c.r)
	id := vars["id"]
	pollMeta, err := pr.get(c, id)
	if err != nil {
		return err
	}
	c.c.Infof("pollMeta: %v", pollMeta)
	//if this is the current user's poll, tweet it otherwise retweet it
	if pollMeta.User.KeyStr == c.u.KeyStr {
		return pollTweet(c, pollMeta)
	} else {
		return pollRetweet(c, pollMeta)
	}
}

func pollTweet(c *context, pollMeta PollMeta) error {
	var pr PollRepository
	//get poll image
	img, err := getImageBytes(c, pollMeta.Question)
	if err != nil {
		return err
	}
	//upload the image to twitter
	var tc TwitterController
	var jsonResp JsonResponse
	jsonResp, err = tc.ApiMediaUpload(c, img)
	if err != nil {
		return err
	}
	//now tweet the link to the poll with the uploaded image
	jsonResp, err = tc.ApiStatusesUpdate(c, pollMeta.Description+" @pollcards "+pollMeta.Url(c), jsonResp["media_id_string"].(string))
	if err != nil {
		return err
	}
	//now update the poll in the datastore with the id_str value of the tweet (for later retweeting)
	pollMeta.TwitterId = jsonResp["id_str"].(string)
	_, err = pr.update(c, pollMeta)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonResp.Bytes())
	return nil
}

func pollRetweet(c *context, pollMeta PollMeta) error {
	if len(pollMeta.TwitterId) == 0 {
		return errors.New("Cannot retweet this Poll. It does not have a corresponding tweet ID value.")
	}
	var tc TwitterController
	//retweet the poll
	jsonResp, err := tc.ApiStatusesRetweet(c, pollMeta.TwitterId)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonResp.Bytes())
	return nil
}

func pollSearchHandler(c *context) error {
	var pr PollRepository
	pollsMeta, err := pr.search(c)
	if err != nil {
		return err
	}
	jsonStr, err := json.Marshal(pollsMeta)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}
