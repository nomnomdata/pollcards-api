package app

import (
	"appengine/memcache"
	"appengine/urlfetch"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/garyburd/go-oauth/oauth"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"net/url"
)

//twitter authentication URLs
var oauthClient = oauth.Client{
	TemporaryCredentialRequestURI: "https://api.twitter.com/oauth/request_token",
	ResourceOwnerAuthorizationURI: "https://api.twitter.com/oauth/authenticate",
	TokenRequestURI:               "https://api.twitter.com/oauth/access_token",
}

type TwitterController struct{}

type TwitterInfo struct {
	IdStr      string
	ScreenName string
	Name       string
	Language   string
	OauthCreds oauth.Credentials
}

type connectInfo struct {
	Secret   string
	Redirect string
}

// gets the OAuth temp credentials and redirects the user to the
// twitter authentication page if the user has not already authorized this app.
func serveTwitterLogin(c *context) error {
	httpClient := urlfetch.Client(c.c)
	callback := "http://" + c.r.Host + "/user/login/twitter/callback"
	tempCred, err := oauthClient.RequestTemporaryCredentials(httpClient, callback, nil)
	if err != nil {
		return err
	}

	ci := connectInfo{Secret: tempCred.Secret, Redirect: c.r.FormValue("redirect")}
	err = memcache.Gob.Set(c.c, &memcache.Item{Key: tempCred.Token, Object: &ci})
	if err != nil {
		return err
	}
	http.Redirect(c.w, c.r, oauthClient.AuthorizationURL(tempCred, nil), 302)
	return nil
}

// handles callbacks from the Twitter OAuth server, requests permanent Twitter Oauth credentials,
// and creates a new user in the datastore if a user with these twitter credentials doesn't already exist.
func serveTwitterCallback(c *context) error {
	token := c.r.FormValue("oauth_token")
	var ci connectInfo
	_, err := memcache.Gob.Get(c.c, token, &ci)
	if err != nil {
		return err
	}
	memcache.Delete(c.c, token)
	tempCred := &oauth.Credentials{
		Token:  token,
		Secret: ci.Secret,
	}
	//get permanent Twitter Oauth credentials
	httpClient := urlfetch.Client(c.c)
	tokenCred, _, err := oauthClient.RequestToken(httpClient, tempCred, c.r.FormValue("oauth_verifier"))
	if err != nil {
		return err
	}
	//if current user info not provided in auth header, request current user's info from twitter (which includes unique twitter id)
	var ur UserRepository
	err = ur.getUserFromAuthHeader(c)
	if err != nil {
		httpClient = urlfetch.Client(c.c)
		resp, err := oauthClient.Get(httpClient, tokenCred, "https://api.twitter.com/1.1/account/verify_credentials.json", nil)
		if err != nil {
			return err
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			p, _ := ioutil.ReadAll(resp.Body)
			return fmt.Errorf("get %s returned status %d, %s", resp.Request.URL, resp.StatusCode, p)
		}
		var verifyCredentials map[string]interface{}
		if err := json.NewDecoder(resp.Body).Decode(&verifyCredentials); err != nil {
			return err
		}
		twitterInfo := &TwitterInfo{
			IdStr:      verifyCredentials["id_str"].(string),
			ScreenName: verifyCredentials["screen_name"].(string),
			Name:       verifyCredentials["name"].(string),
			Language:   verifyCredentials["lang"].(string),
			OauthCreds: *tokenCred,
		}
		// now try to get current user's info from the datastore based on the user's unique twitter id
		// (or create a new user entity if it doesn't already exist)
		var ur UserRepository
		err = ur.getUserFromTwitter(c, twitterInfo)
	}
	//redirect to the referring web page
	http.Redirect(c.w, c.r, ci.Redirect+"?auth="+c.u.KeyStr, 302)
	return nil
}

func (tc TwitterController) ApiStatusesUpdate(c *context, status string, media_ids string) (JsonResponse, error) {
	httpClient := urlfetch.Client(c.c)
	v := url.Values{}
	v.Set("status", status)
	if len(media_ids) > 0 {
		v.Set("media_ids", media_ids)
	}
	resp, err := oauthClient.Post(httpClient, &c.u.TwitterInfo.OauthCreds, "https://api.twitter.com/1.1/statuses/update.json", v)
	if err != nil {
		return nil, err
	}
	var jsonResp JsonResponse
	if err := json.NewDecoder(resp.Body).Decode(&jsonResp); err != nil {
		return nil, err
	}
	return jsonResp, nil
}
func (tc TwitterController) ApiStatusesRetweet(c *context, id string) (JsonResponse, error) {
	httpClient := urlfetch.Client(c.c)
	urlStr := "https://api.twitter.com/1.1/statuses/retweet/" + id + ".json"
	c.c.Infof("urlStr: %v", urlStr)
	resp, err := oauthClient.Post(httpClient, &c.u.TwitterInfo.OauthCreds, urlStr, nil)
	if err != nil {
		return nil, err
	}
	var jsonResp JsonResponse
	if err := json.NewDecoder(resp.Body).Decode(&jsonResp); err != nil {
		return nil, err
	}
	return jsonResp, nil
}

func (tc TwitterController) ApiMediaUpload(c *context, img []byte) (JsonResponse, error) {
	//create buffer to hold the request body
	body_buf := &bytes.Buffer{}
	//create multipart form writer to write to the request body buffer
	body_writer := multipart.NewWriter(body_buf)
	//create header manually for the image part
	mh := make(textproto.MIMEHeader)
	mh.Set("Content-Disposition", "form-data; name=\"media\"; filename=\"image.jpg\"")
	mh.Set("Content-Type", "application/octet-stream")
	part_writer, err := body_writer.CreatePart(mh)
	if err != nil {
		return nil, err
	}
	//copy the image bytes to the request body buffer
	io.Copy(part_writer, bytes.NewBuffer(img))
	//close the writer before creating the request
	body_writer.Close()
	//create the post request with the multipart request body
	req, err := http.NewRequest("POST", "http://upload.twitter.com/1.1/media/upload.json", body_buf)
	if err != nil {
		return nil, err
	}
	//add the multipart header to the request, as well as the twitter auth header
	req.Header.Add("Content-Type", body_writer.FormDataContentType())
	req.Header.Set("Authorization", oauthClient.AuthorizationHeader(&c.u.TwitterInfo.OauthCreds, "POST", req.URL, nil))
	//create the http client and then initiate the request
	httpClient := urlfetch.Client(c.c)
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	var jsonResp JsonResponse
	if err := json.NewDecoder(resp.Body).Decode(&jsonResp); err != nil {
		return nil, err
	}
	return jsonResp, nil
}
