package app

import (
	"appengine"
	"appengine/datastore"
	"errors"
	"github.com/nu7hatch/gouuid"
	"time"
)

type UserInfo struct {
	FirstName   string
	LastName    string
	Email       string
	Credits     int
	CreatedOn   time.Time
	TwitterInfo TwitterInfo
	IsAdmin     bool
}

func (u UserInfo) CreatedOnFmt() string {
	return u.CreatedOn.Format("01/02/2006 15:04")
}

type UserProfileMeta struct {
	Key    *datastore.Key `json:"-"`
	KeyStr string
	UserInfo
}

type UserMeta struct {
	Key               *datastore.Key `json:"-"`
	KeyStr            string
	Name              string
	TwitterScreenName string
}

type UserRepository struct {
}

// returns the current user from the datastore user's uuid key stored in the auth cookie.
func (ur UserRepository) getUserFromAuthHeader(c *context) error {
	var u UserInfo
	auth := c.r.Header.Get("Authorization")
	if len(auth) == 0 {
		return errors.New("No authorization header found")
	}
	key := datastore.NewKey(c.c, "user", auth, 0, nil)
	err := datastore.Get(c.c, key, &u)
	if err != nil {
		return err
	}
	c.u = createUserProfileMeta(c, key, u)
	return nil
}

// returns the current user from their unique twitter id.
func (ur UserRepository) getUserFromTwitter(c *context, t *TwitterInfo) error {
	var u UserInfo
	q := datastore.NewQuery("user").Filter("TwitterInfo.IdStr =", t.IdStr).Limit(1)
	i := q.Run(c.c)
	key, err := i.Next(&u)
	// if the user doesn't exist in the datastore, create a new user from the unique twitter id and ancillary twitter info
	if err != nil {
		var ur UserRepository
		if err := ur.createUser(c, func(u *UserInfo) {
			u.CreatedOn = time.Now()
			u.Credits = appConfig.CreditConfig.NewUser
			u.TwitterInfo = *t
		}); err != nil {
			return err
		}
	}
	c.u = createUserProfileMeta(c, key, u)
	return err
}

func (ur UserRepository) getUser(c *context, key *datastore.Key) (UserMeta, error) {
	var u UserInfo
	err := datastore.Get(c.c, key, &u)
	if err != nil {
		return UserMeta{}, err
	}
	return createUserMeta(c, key, u), nil
}

//create a new user from their twitter info
func (ur UserRepository) createUser(c *context, f func(u *UserInfo)) error {
	//create new uuid (to be used as the datastore user key)
	u4, err := uuid.NewV4()
	if err != nil {
		return err
	}
	u4Str := u4.String()
	//create new user object and add to datastore
	key := datastore.NewKey(c.c, "user", u4Str, 0, nil)
	return datastore.RunInTransaction(c.c, func(ctx appengine.Context) error {
		var u UserInfo
		f(&u)
		_, err = datastore.Put(ctx, key, &u)
		if err != nil {
			return err
		}
		c.u = createUserProfileMeta(c, key, u)
		return nil
	}, nil)
}

func createUserProfileMeta(c *context, key *datastore.Key, u UserInfo) UserProfileMeta {
	UserProfileMeta := UserProfileMeta{
		Key:      key,
		KeyStr:   key.StringID(), //need a separate field for this for json marshalling, as the datastore key is marshalled with the entity type
		UserInfo: u,
	}
	return UserProfileMeta
}

func createUserMeta(c *context, key *datastore.Key, u UserInfo) UserMeta {
	UserMeta := UserMeta{
		Key:               key,
		KeyStr:            key.StringID(),
		Name:              u.TwitterInfo.Name,
		TwitterScreenName: u.TwitterInfo.ScreenName,
	}
	return UserMeta
}
