package app

import (
	"appengine/datastore"
	"errors"
	"github.com/nu7hatch/gouuid"
	"time"
)

type Vote struct {
	PollKey   *datastore.Key `json:"-"`
	UserKey   *datastore.Key `json:"-"`
	AnswerIdx int            //index into Answers array
	CreatedOn time.Time
}

type VoteMeta struct {
	KeyStr     string
	PollKeyStr string
	UserKeyStr string
	Vote
}

func (v VoteMeta) CreatedOnFmt() string {
	return v.CreatedOn.Format("01/02/2006 15:04")
}

type VoteRepository struct {
}

//vote on a poll
func (repos VoteRepository) create(c *context, pollKeyStr string, answerIdx int) (VoteMeta, error) {
	//make sure this isn't my poll (you can't vote on your own poll)
	pollKey := datastore.NewKey(c.c, "poll", pollKeyStr, 0, nil)
	q := datastore.NewQuery("poll").Filter("__key__ =", pollKey).Filter("UserKey =", c.u.Key).KeysOnly()
	count, err := q.Count(c.c)
	if count > 0 {
		return VoteMeta{}, errors.New("You can't vote on your own poll")
	}
	//try to get a vote for the specified poll this user
	q = datastore.NewQuery("vote").Filter("PollKey =", pollKey).Filter("UserKey =", c.u.Key).Limit(1)
	t := q.Run(c.c)
	var v Vote
	key, err := t.Next(&v)
	isVoteChanged := false
	isNewVote := false
	//if the user already voted for this poll, change their vote if necessary
	if key != nil {
		if v.AnswerIdx != answerIdx {
			isVoteChanged = true
			v.AnswerIdx = answerIdx
			v.CreatedOn = time.Now()
		}
		//otherwise this is a new vote by this user for this poll
	} else {
		isNewVote = true
		u4, err := uuid.NewV4()
		if err != nil {
			return VoteMeta{}, err
		}
		u4Str := u4.String()
		key = datastore.NewKey(c.c, "vote", u4Str, 0, nil)
		v.PollKey = pollKey
		v.UserKey = c.u.Key
		v.AnswerIdx = answerIdx
		v.CreatedOn = time.Now()
	}
	//write to the datastore only if it's a new vote or the vote was changed
	if isNewVote || isVoteChanged {
		_, err = datastore.Put(c.c, key, &v)
		if err != nil {
			return VoteMeta{}, err
		}
		//adjust the user's credits for voting
		if isNewVote {
			adjustCredits(c, appConfig.CreditConfig.Vote)
		}
	}
	//create and return the vote and associated metadata
	voteMeta, err := createVoteMeta(c, key, v)
	if err != nil {
		return VoteMeta{}, err
	}
	return voteMeta, nil
}

func (repos VoteRepository) getAll(c *context, pollKey *datastore.Key) ([]VoteMeta, error) {
	var votes []Vote
	q := datastore.NewQuery("vote").Filter("PollKey =", pollKey)
	keys, err := q.GetAll(c.c, &votes)
	if err != nil {
		return nil, err
	}
	votesMeta := make([]VoteMeta, len(keys))
	for i, v := range votes {
		voteMeta, err := createVoteMeta(c, keys[i], v)
		if err != nil {
			return nil, err
		}
		votesMeta[i] = voteMeta
	}
	return votesMeta, nil
}

func (repos VoteRepository) getAllByAnswer(c *context, pollKey *datastore.Key, answerIdx int) ([]VoteMeta, error) {
	var votes []Vote
	q := datastore.NewQuery("vote").Filter("PollKey =", pollKey).Filter("AnswerIdx =", answerIdx)
	keys, err := q.GetAll(c.c, &votes)
	if err != nil {
		return nil, err
	}
	votesMeta := make([]VoteMeta, len(keys))
	for i, v := range votes {
		voteMeta, err := createVoteMeta(c, keys[i], v)
		if err != nil {
			return nil, err
		}
		votesMeta[i] = voteMeta
	}
	return votesMeta, nil
}

func createVoteMeta(c *context, key *datastore.Key, v Vote) (VoteMeta, error) {
	voteMeta := VoteMeta{
		KeyStr:     key.StringID(),
		PollKeyStr: v.PollKey.StringID(),
		UserKeyStr: v.UserKey.StringID(),
		Vote:       v,
	}
	return voteMeta, nil
}
