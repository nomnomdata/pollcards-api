package app

import (
	"encoding/json"
	"errors"
	"github.com/gorilla/mux"
	"strconv"
)

func voteCreateHandler(c *context) error {
	var repos VoteRepository
	vars := mux.Vars(c.r)
	pollId := vars["pollId"]
	answerIdx, err := strconv.Atoi(vars["answerIdx"])
	if err != nil {
		return errors.New("Answer Id must be an integer value >= 0")
	}
	meta, err := repos.create(c, pollId, answerIdx)
	if err != nil {
		return err
	}
	jsonStr, err := json.Marshal(meta)
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(jsonStr)
	return nil
}

func voteTweetHandler(c *context) error {
	vars := mux.Vars(c.r)
	pollId := vars["pollId"]
	answerIdx, err := strconv.Atoi(vars["answerIdx"])
	if err != nil {
		return err
	}
	var pollRepos PollRepository
	pollMeta, err := pollRepos.get(c, pollId)
	if err != nil {
		return err
	}
	var tc TwitterController
	json, err := tc.ApiStatusesUpdate(c, "I voted: "+pollMeta.Answers[answerIdx].Answer+". Vote here: "+pollMeta.Url(c), "")
	if err != nil {
		return err
	}
	c.w.Header().Set("Content-type", "application/json")
	c.w.WriteHeader(200)
	c.w.Write(json.Bytes())

	return nil
}
